import {Component} from '@angular/core';
import {environment as env} from '../environments/environment'


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'ch2-kickstart-alt';
    year: number = new Date().getFullYear();
    production: boolean = env.production;
}
