import {Component} from '@angular/core';

interface Link {
    uri: string;
    label: string;
}

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    links: Link[] = [
        {uri:'/home', label: 'Start Page'},
        {uri:'/about', label: 'About this app'},
        {uri:'/products', label: 'All Products'},
    ];
}
