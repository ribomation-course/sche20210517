import {NgModule} from '@angular/core';
import {RouterModule, Route} from '@angular/router';

import {HomePage} from "./pages/home/home.page";
import {AboutPage} from "./pages/about/about.page";
import {ProductsPage} from "./pages/products/products.page";
import { NotFoundPage } from './pages/not-found/not-found.page';
import {ProductViewPage} from "./pages/products/product-view/product-view.page";

const routes: Route[] = [
    {path: 'home', component: HomePage},
    {path: 'about', component: AboutPage},
    {path: 'products', component: ProductsPage},
    {path: 'product/:id', component: ProductViewPage},
    {path: '', redirectTo: '/home', pathMatch: 'full'},
    {path: '**', component: NotFoundPage},
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
