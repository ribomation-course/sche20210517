import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HomePage} from './pages/home/home.page';
import {AboutPage} from './pages/about/about.page';
import {ProductsPage} from './pages/products/products.page';
import {NotFoundPage} from './pages/not-found/not-found.page';
import { ProductViewPage } from './pages/products/product-view/product-view.page';

@NgModule({
    declarations: [
        AppComponent,
        HomePage,
        AboutPage,
        ProductsPage,
        NotFoundPage,
        ProductViewPage
    ],
    imports: [
        BrowserModule,
        AppRoutingModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
