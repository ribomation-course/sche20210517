import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Product} from "../../../product.domain";
import {ProductsDaoService} from "../products-dao.service";
import {Title} from "@angular/platform-browser";

@Component({
    selector: 'app-product-view',
    templateUrl: './product-view.page.html',
    styleUrls: ['./product-view.page.scss']
})
export class ProductViewPage implements OnInit {
    product: Product | undefined;

    constructor(
        private route: ActivatedRoute,
        private dao: ProductsDaoService,
        private titleSvc: Title)
    {
        titleSvc.setTitle('Product | Routing-App');
    }

    ngOnInit(): void {
        const id = this.route.snapshot.paramMap.get('id');
        if (id) {
            this.product = this.dao.findById(+id);
            this.titleSvc.setTitle(`${this.product?.name} | Routing-App`);
        }
    }
}
