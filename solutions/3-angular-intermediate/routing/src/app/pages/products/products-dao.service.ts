import {Injectable} from '@angular/core';
import {mkProduct, Product} from "../../product.domain";

@Injectable({
    providedIn: 'root'
})
export class ProductsDaoService {
    products: Product[] = [];

    constructor() {
        let n = 10;
        while (n-- > 0) {
            this.products.push(mkProduct());
        }
    }

    findAll(): Product[] {
        return this.products;
    }

    findById(id: number): Product | undefined {
        return this.products.find(p => p.id === id);
    }

}
