import {Component} from '@angular/core';
import {ProductsDaoService} from "./products-dao.service";
import {Title} from "@angular/platform-browser";

@Component({
    selector: 'app-products',
    templateUrl: './products.page.html',
    styleUrls: ['./products.page.scss']
})
export class ProductsPage {
    constructor(
        public dao: ProductsDaoService,
        private titleSvc: Title)
    {
        titleSvc.setTitle('Products | Routing-App');
    }
}
