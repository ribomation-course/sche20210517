import {Component, OnInit} from '@angular/core';
import {Title} from "@angular/platform-browser";

@Component({
    selector: 'app-not-found',
    templateUrl: './not-found.page.html',
    styleUrls: ['./not-found.page.scss']
})
export class NotFoundPage implements OnInit {

    constructor(private titleSvc: Title) {
        titleSvc.setTitle('Not Found');
    }

    ngOnInit(): void {
    }

}
