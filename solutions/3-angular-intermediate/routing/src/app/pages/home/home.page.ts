import {Component} from '@angular/core';
import {Title} from "@angular/platform-browser";

@Component({
    selector: 'app-home',
    template: `
        <h1>Welcome</h1>
        <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos ipsam, suscipit! Inventore quae quaerat quod
            similique
            vel? At fugit incidunt laudantium officiis quas, quo reprehenderit veritatis? Provident ratione repellat
            vero?
        </p>
    `,
    styles: ['']
})
export class HomePage {
    constructor(private titleSvc: Title) {
        titleSvc.setTitle('Home');
    }
}
