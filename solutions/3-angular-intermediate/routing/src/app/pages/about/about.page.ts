import { Component, OnInit } from '@angular/core';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-about',
  templateUrl: './about.page.html',
  styleUrls: ['./about.page.scss']
})
export class AboutPage implements OnInit {

  constructor(private titleSvc: Title) {
      titleSvc.setTitle('About')
  }

  ngOnInit(): void {
  }

}
