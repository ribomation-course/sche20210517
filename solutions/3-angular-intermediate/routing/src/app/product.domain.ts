export interface Product {
    id: number;
    name: string;
    price: number;
}

let nextId = 1;

export function mkProduct(): Product {
    const names = [
        "apple", "banana", "coco nut", "date plum",
        "orange", "pear", "grapes"
    ]

    return {
        id: nextId++,
        name: names[Math.floor(names.length * Math.random())],
        price: 100 + Math.random() * 1000,
    }
}

