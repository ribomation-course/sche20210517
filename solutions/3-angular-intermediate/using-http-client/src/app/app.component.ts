import {Component} from '@angular/core';
import {UsersService} from "./users.service";
import {User} from "./user.domain";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    users: User[] = [];
    oneUser: User | undefined;

    constructor(public userSvc: UsersService) {
        userSvc.findAll().subscribe(objs => this.users = objs);
    }

    load(id: number) {
        this.userSvc.findById(id).subscribe(obj => this.oneUser = obj)
    }

    reset() {
        this.oneUser = undefined;
    }
}
