import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {User} from "./user.domain";

@Injectable({
    providedIn: 'root'
})
export class UsersService {
    private readonly url = 'http://localhost:3000/users';

    constructor(private http: HttpClient) {
    }

    findAll(): Observable<User[]> {
        return this.http.get<User[]>(this.url);
    }

    findById(id: number): Observable<User> {
        return this.http.get<User>(this.url + '/' + id);
    }

}
