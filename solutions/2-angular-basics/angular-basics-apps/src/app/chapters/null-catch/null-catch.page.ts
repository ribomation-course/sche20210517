import {Component} from '@angular/core';

@Component({
    selector: 'app-null-catch',
    templateUrl: './null-catch.page.html',
    styleUrls: ['./null-catch.page.scss']
})
export class NullCatchPage {
    user: any = {
        name: {
            first: 'Anna',
            last: 'Conda'
        }
    }

    toggle() {
        if (this.user) {
            this.user = undefined;
        } else {
            this.user = {
                name: {
                    first: 'Per',
                    last: 'Silja'
                }
            }
        }
    }
}
