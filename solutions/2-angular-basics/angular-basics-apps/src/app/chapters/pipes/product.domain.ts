export interface Product {
    name: string;
    price: number;
    count: number;
    service: boolean;
    updated: Date;
}

export function mkProduct(): Product {
    const names = [
        "apple", "banana", "coco nut", "date plum",
        "orange", "pear", "grapes"
    ]

    return {
        name: names[Math.floor(names.length * Math.random())],
        price: 100 + Math.random() * 1000,
        count: Math.random() * 10,
        service: Math.random() < .5,
        updated: new Date(Date.now() - Math.random() * 90 * 24 * 3600)
    }
}


