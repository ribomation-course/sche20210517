import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'kr'
})
export class KrPipe implements PipeTransform {

    transform(value: number, showOren: boolean = false): string {
        if (!value) {
            return '';
        }
        return (+value).toLocaleString('sv-SE', {
            useGrouping: true,
            minimumFractionDigits: showOren ? 2 : 0,
            maximumFractionDigits: showOren ? 2 : 0,
        }) + ' kr';
    }

}
