import {Component, OnInit} from '@angular/core';
import {mkProduct, Product} from "./product.domain";

@Component({
    selector: 'app-pipes',
    templateUrl: './pipes.page.html',
    styleUrls: ['./pipes.page.scss']
})
export class PipesPage implements OnInit {
    products: Product[] = [];

    ngOnInit(): void {
        let n=10;
        while (n-- > 0) {
            this.products.push(mkProduct());
        }
    }

}
