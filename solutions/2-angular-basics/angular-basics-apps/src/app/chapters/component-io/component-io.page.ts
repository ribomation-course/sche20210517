import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-component-io',
    templateUrl: './component-io.page.html',
    styleUrls: ['./component-io.page.scss']
})
export class ComponentIoPage implements OnInit {
    fontSize: number = 0;
    text: string = 'Tjolla Hopp';

    ngOnInit(): void {
        this.reset();
    }

    reset() {
        this.fontSize = 15;
    }
}
