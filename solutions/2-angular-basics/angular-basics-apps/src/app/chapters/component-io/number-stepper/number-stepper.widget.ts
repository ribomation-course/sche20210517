import {EventEmitter, Component, Input, Output} from '@angular/core';

@Component({
    selector: 'number-stepper',
    templateUrl: './number-stepper.widget.html',
    styleUrls: ['./number-stepper.widget.scss']
})
export class NumberStepperWidget {
    @Input('value') current: number | string = 0;
    @Output('valueChange') out = new EventEmitter<number>();

    dec() {
        this.resize(-1);
    }

    inc() {
        this.resize(+1);
    }

    resize(delta: number) {
        const nextValue = +this.current + delta;
        const minValue = 1;
        const maxValue = 50;
        this.current = Math.min(maxValue, Math.max(minValue, nextValue));
        this.out.emit(this.current);
    }
}
