import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-components',
    templateUrl: './components.page.html',
    styleUrls: ['./components.page.scss']
})
export class ComponentsPage  {
    name: string = 'Anna Conda';
    email: string = 'anna@gmail.com';

    onSave(value:any) {
        console.log('[onSave] value: %o', value)
    }

}
