import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
    selector: 'editable-text',
    templateUrl: './editable-text.widget.html',
    styleUrls: ['./editable-text.widget.scss']
})
export class EditableTextWidget {
    @Input('value') label: string = '';
    @Output('valueChange') labelEmitter = new EventEmitter<string>();
    @Input('hint') hint: string = 'Click to edit';

    formVisible: boolean = false;
    formValue: string = '';

    get empty(): boolean {
        return this.label === undefined || this.label === null || this.label.toString().trim().length === 0;
    }

    edit() {
        this.formValue = this.label;
        this.formVisible = true;
    }

    onKeyUp(evt: any) {
        if (evt.key === 'Escape') {
            this.formVisible = false;
        } else if (evt.key === 'Enter') {
            this.formVisible = false;
            this.label = this.formValue;
            this.labelEmitter.emit(this.label);
        }
    }

}
