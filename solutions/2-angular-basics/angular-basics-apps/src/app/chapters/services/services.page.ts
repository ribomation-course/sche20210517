import {Component, OnInit} from '@angular/core';
import {ProductsService} from "./products.service";
import {LoggerService} from "./logger.service";

@Component({
    selector: 'app-services',
    templateUrl: './services.page.html',
    styleUrls: ['./services.page.scss']
})
export class ServicesPage implements OnInit {

    constructor(public svc: ProductsService, public log: LoggerService) {
    }

    ngOnInit(): void {
        this.log.print('services page loaded');
    }

}
