import {Injectable} from '@angular/core';

export interface Product {
    name: string;
    price: number;
}


@Injectable()
export class ProductsService {
    items: Product[] = [];

    findAll():Product[] {
        return this.items;
    }
}
