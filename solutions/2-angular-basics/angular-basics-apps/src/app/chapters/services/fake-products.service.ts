import {Injectable} from '@angular/core';
import {Product, ProductsService} from "./products.service";
import {LoggerService} from "./logger.service";

function mkProduct(): Product {
    const names = [
        'apple', 'banana', 'coco nut', 'date plum', 'orange'
    ]
    return {
        name: names[Math.floor(names.length * Math.random())],
        price: 10 + Math.random() * 100,
    };
}

@Injectable()
export class FakeProductsService extends ProductsService {
    fakeProducts: Product[] = [];

    constructor(private log: LoggerService) {
        super();
        let n = 10;
        log.print('generating ' + n + ' products');
        while (n-- > 0) this.fakeProducts.push(mkProduct());
    }

    findAll(): Product[] {
        this.log.print('findAll()');
        return this.fakeProducts;
    }

}
