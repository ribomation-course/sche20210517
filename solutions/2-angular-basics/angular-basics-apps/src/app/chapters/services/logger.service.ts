import {Injectable} from '@angular/core';
import {formatDate} from '@angular/common';

export function LoggerServiceFactory() {
    return new LoggerService(false);
}

// @Injectable()
export class LoggerService {
    quiet: boolean = false;

    constructor(quiet: boolean) {
        this.quiet = quiet;
    }

    print(msg: string) {
        if (!this.quiet) {
            console.warn('[%s] %s', formatDate(new Date(), 'HH:mm:ss', 'en'), msg)
        }
    }
}
