import {Component, OnInit} from '@angular/core';

interface Person {
    first_name: string;
    last_name: string;
    email: string;
}

@Component({
    selector: 'app-directives',
    templateUrl: './directives.page.html',
    styleUrls: ['./directives.page.scss']
})
export class DirectivesPage implements OnInit {
    persons: Person[] = [{
        "first_name": "Terrie",
        "last_name": "D'Angeli",
        "email": "tdangeli0@nifty.com"
    }, {
        "first_name": "Zoe",
        "last_name": "Peirone",
        "email": "zpeirone1@guardian.co.uk"
    }, {
        "first_name": "Ulrica",
        "last_name": "Beverley",
        "email": "ubeverley2@ehow.com"
    }, {
        "first_name": "Brandyn",
        "last_name": "Longden",
        "email": "blongden3@live.com"
    }, {
        "first_name": "Kellyann",
        "last_name": "Fosten",
        "email": "kfosten4@squarespace.com"
    }, {
        "first_name": "Rowney",
        "last_name": "Worling",
        "email": "rworling5@illinois.edu"
    }, {
        "first_name": "Sashenka",
        "last_name": "Livett",
        "email": "slivett6@edublogs.org"
    }, {
        "first_name": "Paula",
        "last_name": "Cristofaro",
        "email": "pcristofaro7@nationalgeographic.com"
    }, {
        "first_name": "Cathie",
        "last_name": "Skidmore",
        "email": "cskidmore8@hugedomains.com"
    }, {
        "first_name": "Corey",
        "last_name": "Dowker",
        "email": "cdowker9@cmu.edu"
    }];

    constructor() {
    }

    ngOnInit(): void {
    }

}
