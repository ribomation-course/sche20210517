import {Component} from '@angular/core';

interface Link {
    title: string;
    uri: string;
}

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    chapters: Link[] = [
        {title: 'Ch1: Template Syntax', uri: '/chapter-1'},
        {title: 'Ch2: Pipes', uri: '/chapter-2'},
        {title: 'Ch3: Directives', uri: '/chapter-3'},
        {title: 'Ch4: Services', uri: '/chapter-4'},
        {title: 'Ch5: Component I/O', uri: '/chapter-5'},
        {title: 'Ch6: Components', uri: '/chapter-6'},
    ]
}
