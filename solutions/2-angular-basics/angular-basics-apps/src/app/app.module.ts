import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NullCatchPage} from './chapters/null-catch/null-catch.page';
import {PipesPage} from './chapters/pipes/pipes.page';
import {KrPipe} from './chapters/pipes/kr.pipe';
import {DirectivesPage} from './chapters/directives/directives.page';
import {ServicesPage} from './chapters/services/services.page';
import {ProductsService} from "./chapters/services/products.service";
import {FakeProductsService} from "./chapters/services/fake-products.service";
import {LoggerService, LoggerServiceFactory} from "./chapters/services/logger.service";
import { ComponentIoPage } from './chapters/component-io/component-io.page';
import { NumberStepperWidget } from './chapters/component-io/number-stepper/number-stepper.widget';
import { ComponentsPage } from './chapters/components/components.page';
import { EditableTextWidget } from './chapters/components/editable-text/editable-text.widget';
import {FormsModule} from "@angular/forms";

@NgModule({
    declarations: [
        AppComponent,
        NullCatchPage,
        PipesPage,
        KrPipe,
        DirectivesPage,
        ServicesPage,
        ComponentIoPage,
        NumberStepperWidget,
        ComponentsPage,
        EditableTextWidget
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule
    ],
    providers: [
        {provide: ProductsService, useClass: FakeProductsService},
        {provide: LoggerService, useFactory: LoggerServiceFactory}
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
