import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {NullCatchPage} from "./chapters/null-catch/null-catch.page";
import {PipesPage} from "./chapters/pipes/pipes.page";
import {DirectivesPage} from "./chapters/directives/directives.page";
import {ServicesPage} from "./chapters/services/services.page";
import {ComponentIoPage} from "./chapters/component-io/component-io.page";
import {ComponentsPage} from "./chapters/components/components.page";

const routes: Routes = [
    {path: 'chapter-1', component: NullCatchPage},
    {path: 'chapter-2', component: PipesPage},
    {path: 'chapter-3', component: DirectivesPage},
    {path: 'chapter-4', component: ServicesPage},
    {path: 'chapter-5', component: ComponentIoPage},
    {path: 'chapter-6', component: ComponentsPage},
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
