import {Component} from '@angular/core';

interface Link {
    uri: string;
    label: string;
}

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    links: Link[] = [
        {uri: '/date-offset', label: 'Ch1: Form Fields'},
        {uri: '/registration-form', label: 'Ch 3-4: Registration Form'},
    ];
}
