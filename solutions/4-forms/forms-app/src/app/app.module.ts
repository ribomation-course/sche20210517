import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DateOffsetPage } from './pages/date-offset/date-offset.page';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { RegistrationFormPage } from './pages/registration-form/registration-form.page';
import { TemplateOrientedFormWidget } from './pages/registration-form/template-oriented-form/template-oriented-form.widget';
import { ComponentOrientedFormWidget } from './pages/registration-form/component-oriented-form/component-oriented-form.widget';

@NgModule({
  declarations: [
    AppComponent,
    DateOffsetPage,
    RegistrationFormPage,
    TemplateOrientedFormWidget,
    ComponentOrientedFormWidget
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
