import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DateOffsetPage} from './pages/date-offset/date-offset.page';
import {RegistrationFormPage} from "./pages/registration-form/registration-form.page";

const routes: Routes = [
    {path: 'date-offset', component: DateOffsetPage},
    {path: 'registration-form', component: RegistrationFormPage},
    {path: '', redirectTo: '/date-offset', pathMatch: 'full'},
    {path: '**', redirectTo: '/', pathMatch: 'full'},
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
