import {Component} from '@angular/core';
import {formatDate} from "@angular/common";


@Component({
    selector: 'app-date-offset',
    templateUrl: './date-offset.page.html',
    styleUrls: ['./date-offset.page.scss']
})
export class DateOffsetPage {
    date: Date = new Date();
    offset: number = 5;
    minOffset: number = 0;
    maxOffset: number = 42;

    nextDate(date: Date, offset: number): Date {
        const DAY_MS = 24 * 3600 * 1000;
        return new Date(date.getTime() + offset * DAY_MS);
    }

    toHTML(value: Date): string {
        return formatDate(value, 'yyyy-MM-dd', 'en');
    }

    fromHTML(value: string): Date {
        return new Date(value)
    }
}
