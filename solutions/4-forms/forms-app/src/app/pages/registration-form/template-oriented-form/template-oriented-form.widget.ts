import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {User} from "../user.domain";
import {FormGroup} from "@angular/forms";

@Component({
    selector: 'template-oriented-form',
    templateUrl: './template-oriented-form.widget.html',
    styleUrls: ['./template-oriented-form.widget.scss']
})
export class TemplateOrientedFormWidget {
    initValue: User = {email: ''};
    user: User = {email: ''};
    @Input('init') set value(user: User) {
        this.user = user;
        this.initValue = Object.assign({}, user);
    }
    @Output('submit') submitEmitter = new EventEmitter<User>();
    @Output('cancel') cancelEmitter = new EventEmitter<any>();
    @ViewChild('registrationForm') registrationForm: FormGroup | undefined;

    submit() {
        this.submitEmitter.emit(this.user);
        this.registrationForm?.reset();
        this.registrationForm?.setValue(this.initValue);
    }

    cancel() {
        this.cancelEmitter.emit()
    }
}
