import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {User} from "../user.domain";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
    selector: 'component-oriented-form',
    templateUrl: './component-oriented-form.widget.html',
    styleUrls: ['./component-oriented-form.widget.scss']
})
export class ComponentOrientedFormWidget {
    @Input('init') set value(user: User) {
        this.email.setValue(user.email);
        this.initValue = Object.assign({}, user);
    }
    @Output('submit') submitEmitter = new EventEmitter<User>();
    @Output('cancel') cancelEmitter = new EventEmitter<any>();

    registrationForm: FormGroup;
    email: FormControl;
    initValue: User | undefined;

    constructor() {
        this.email = new FormControl('', [Validators.required, Validators.email]);
        this.registrationForm = new FormGroup({
            email: this.email
        });
    }

    submit() {
        this.submitEmitter.emit(this.registrationForm.value);
        this.registrationForm?.reset();
        this.registrationForm?.setValue(this.initValue as User);
    }

    cancel() {
        this.cancelEmitter.emit()
    }
}
