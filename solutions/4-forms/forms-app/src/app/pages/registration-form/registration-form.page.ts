import {Component, OnInit} from '@angular/core';
import {User} from "./user.domain";

@Component({
    selector: 'app-registration-form',
    templateUrl: './registration-form.page.html',
    styleUrls: ['./registration-form.page.scss']
})
export class RegistrationFormPage {
    user: User = {email: 'nisse.hult@gmail.com'};

    onSubmit(payload:any) {
        console.log('[onSubmit] payload: %o', payload)
    }

    onCancel() {
        console.log('[onCancel]')
    }

}
