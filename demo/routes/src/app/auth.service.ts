import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {Subject} from "rxjs/Subject";

export interface User {
  authenticated: boolean;
  username?: string;
}

@Injectable()
export class AuthService {
  private authenticated: boolean = false;
  private user$: Subject<User>;

  constructor() {
    this.user$ = new Subject<User>();
    this.user$.next({authenticated: true});
  }

  login(usr: string, pwd?: string): void {
    this.authenticated = true;
    this.user$.next({authenticated: true, username: usr});
  }

  logout(): void {
    this.authenticated = false;
    this.user$.next({authenticated: false});
  }

  authFeed(): Observable<User> {
    return this.user$;
  }

  isAuthenticated(): boolean {
    return this.authenticated;
  }
}
