import * as Rx from 'rxjs/Rx';

Rx.Observable.range(1, 20)
    .filter(n => n % 3 !== 0)
    .map(x => x * x)
    .subscribe(n => console.log(n));

