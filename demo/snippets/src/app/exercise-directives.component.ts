import {Component, OnInit} from "@angular/core";

interface Vehicle {
  type: string;
  name: string;
  price: number;
}

@Component({
  selector: "exercise-directives",
  template: `
    <div>
      Number of Vehicles:
      <input type="number" min="0" [(ngModel)]="count"/>
      <button type="button" (click)="update()">
        <i class="fa fa-refresh"></i>
        Update
      </button>
    </div>
    <table *ngIf="vehicles.length > 0">
      <thead>
      <tr>
        <th>ID</th>
        <th>Type</th>
        <th>Name</th>
        <th>Price</th>
      </tr>
      </thead>
      <tbody>
      <tr *ngFor="let v of vehicles; index as id; odd as isOdd" [ngClass]="{'odd':isOdd}">
        <td>{{id + 1}}</td>
        <td>
          <span [ngSwitch]="v.type">
            <i *ngSwitchCase="'taxi'" class="fa fa-taxi"></i>
            <i *ngSwitchCase="'plane'" class="fa fa-plane"></i>
            <i *ngSwitchCase="'bicycle'" class="fa fa-bicycle"></i>
            <i *ngSwitchCase="'truck'" class="fa fa-truck"></i>
          </span>
        </td>
        <td>{{v.name | titlecase}}</td>
        <td>{{v.price | number:'1.2-2'}} kr</td>
      </tr>
      </tbody>
    </table>
    <p *ngIf="vehicles.length === 0" class="warning">
      Sorry, no vehicles here!
    </p>
  `,
  styles: [`
    table {
      width: 20rem;
    }

    td:nth-child(2) {
      text-align: center;
    }

    td:last-child, td:first-child {
      text-align: right;
    }

    .odd {
      background-color: rgba(135, 206, 250, 0.51);
    }

    .warning {
      color: orangered;
    }
  `]
})
export class ExerciseDirectivesComponent implements OnInit {
  vehicles: Vehicle[] = [];
  count: number = 10;

  ngOnInit() {
    this.update();
  }

  update(): void {
    this.vehicles = this.mkLst(this.count);
  }

  private mk(): Vehicle {
    const ix = (arr) => Math.floor(arr.length * Math.random());
    const price = () => Math.random() * 10000 + 999;
    const types = ["taxi", "plane", "bicycle", "truck"];
    const names = ["anna", "berit", "carin", "doris", "eva", "fia", "gun", "hanna"];

    return {
      type: types[ix(types)],
      name: names[ix(names)],
      price: price()
    };
  }

  private mkLst(n: number): Vehicle[] {
    const lst = [];
    while (n-- > 0) lst.push(this.mk());
    return lst;
  }
}
