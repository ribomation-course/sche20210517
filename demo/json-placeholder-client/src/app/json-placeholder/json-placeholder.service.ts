import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Todo} from '../model/todo.model';

@Injectable()
export class JsonPlaceholderService {
  readonly baseUri: string = 'https://jsonplaceholder.typicode.com/todos';

  constructor(private http: HttpClient) {
  }

  list(): Observable<Todo[]> {
    return this.http.get<Todo[]>(this.baseUri);
  }

  get(id: number): Observable<Todo> {
    return this.http.get<Todo>(`${this.baseUri}/${id}`);
  }

}
