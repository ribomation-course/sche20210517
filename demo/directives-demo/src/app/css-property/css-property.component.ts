import { Component } from '@angular/core';

@Component({
  selector: 'app-css-property',
  templateUrl: './css-property.component.html',
  styleUrls: ['./css-property.component.css']
})
export class CssPropertyComponent {
  color: string = '#555';
  size: number = 1;

  background() {
    this.color = (this.color === '#555') ? '#f00' : '#555';
  }

  fontsize() {
    this.size = (this.size === 1) ? 2 : 1;
  }
}


