import { Component, OnInit, ViewChild } from '@angular/core';
import { Product } from '../product.model';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-template-oriented-form',
  templateUrl: './template-oriented-form.component.html',
  styleUrls: ['./template-oriented-form.component.scss']
})
export class TemplateOrientedFormComponent implements OnInit {
  product: Product;
  @ViewChild('productForm', { static: false }) productForm: FormGroup;

  ngOnInit() {
    this.product = { name: 'Banana', price: 42 };
  }

  updateProductName() { this.product.name = 'Orange'; }
  updateProductPrice() { this.product.price = 100; }
  onSubmit() {
    this.productForm.reset({ name: 'Apple', price: 17 });
    alert(`payload: ${JSON.stringify(this.product)}`);
  }
}
