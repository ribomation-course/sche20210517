import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { BannerComponent } from './banner/banner.component';
import { LoginComponent } from './login/login.component';
import { PageHeaderComponent } from './page-header/page-header.component';
import { NumberStepperComponent } from './number-stepper/number-stepper.component';


@NgModule({
  declarations: [
    AppComponent,
    BannerComponent,
    LoginComponent,
    PageHeaderComponent,
    NumberStepperComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
