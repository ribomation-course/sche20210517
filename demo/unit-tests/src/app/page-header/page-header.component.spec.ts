import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PageHeaderComponent} from './page-header.component';
import {AuthService} from '../auth/auth.service';
import {By} from '@angular/platform-browser';

describe('PageHeaderComponent', () => {
  let component: PageHeaderComponent;
  let fixture  : ComponentFixture<PageHeaderComponent>;
  let authSvc  : AuthService;
  let hdrElem  : HTMLElement;

  class MockAuthService extends AuthService {
    isAuth: boolean = false;
    usr: string;
    login(usr: string, pwd: string): boolean {
      this.isAuth = true; this.usr = usr;
      return true;
    }
    isAuthenticated(): boolean {return this.isAuth;}
    logout()         : void    {this.isAuth = false;}
    getUsername()    : string  {return this.usr;}
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PageHeaderComponent],
      providers: [
        {provide: AuthService, useClass: MockAuthService}
      ]
    });

    fixture   = TestBed.createComponent(PageHeaderComponent);
    component = fixture.componentInstance;
    authSvc   = TestBed.get(AuthService);
    hdrElem   = fixture.debugElement.query(
      By.css('div.row:first-child')).nativeElement;
    fixture.detectChanges();
  });

  it('should create the actors', () => {
    expect(component).toBeDefined();
    expect(authSvc).toBeDefined();
    expect(authSvc instanceof MockAuthService).toBeTruthy()
  });

  it('should show login message', () => {
    expect(hdrElem.textContent).toContain('log in');
  });

  it('should show welcome message, after login', () => {
    authSvc.login('per.silja', 'password');
    component.ngOnInit();
    fixture.detectChanges();
    expect(hdrElem.textContent).toContain('Welcome');
    expect(hdrElem.textContent).toContain('silja');
  });

  it('should show login message, after logout', () => {
    expect(hdrElem.textContent).toContain('log in');

    authSvc.login('per.silja', 'password');
    component.ngOnInit();
    fixture.detectChanges();
    expect(hdrElem.textContent).toContain('silja');

    authSvc.logout();
    component.ngOnInit();
    fixture.detectChanges();
    expect(hdrElem.textContent).toContain('log in');
  });

});
